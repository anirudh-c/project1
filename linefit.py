from __future__ import division
import itertools
from functools import reduce

def formLine(filename):
    #Open file
    testfile = open(filename,mode='r',encoding='utf-8')

    #list to hold the lines in testfile and x[i] and y[i]
    lines = []
    x = []
    y = []

    #Fill lines with the lines of the file
    for line in testfile:
        lines.append(line)
    testfile.close()

    #Split the lines around the empty line
    w = '\n'
    f1 = [list(y) for x,y in itertools.groupby(lines, lambda z: z == w) if not x]

    #Assign the two sub-lists to raw x and y data
    x_raw = f1[0]
    y_raw = f1[1]

    #Remove the heading from the raw data
    x_raw.pop(0)
    y_raw.pop(0)

    #Remove the newline characters and place the output into string x and y data
    x_str = []
    y_str = []
    for i in range(len(x_raw)):
        x_str.append(x_raw[i][:-1])
        y_str.append(y_raw[i][:-1])

    #Add the float values into x and y from string x and y
    for i in range(len(x_str)):
        x.append(float(x_str[i]))
        y.append(float(y_str[i]))

    N = len(x)

    #compute the 4 summations and store
    l0 = reduce((lambda a,b: a+b),x)
    l1 = reduce((lambda a,b: a+b),y)
    l2 = reduce((lambda a,b: a+b),[m*n for m,n in zip(x,y)])
    l3 = reduce((lambda a,b: a+b),list(map((lambda p: p*p),x)))

    #compute a and b
    a = (N*l2 - l0*l1)/(N*l3 - l1*l1)
    b = (l1 - l0*a)/N

    print(a,b)
    return a,b

if __name__ == "__main__":
    formLine("test1.txt")
    formLine("test2.txt")
