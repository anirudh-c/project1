from __future__ import division
#Import the main methods for solving the tri-diagonal problem
from main import *
#Import mathematic functions
import math

def formMatrix(N,d):
    """
    This method takes an integer N and returns the three diagonals of the matrix formed by
    discretising the first degree Poisson equation -u'' = f, with N-1 intervals on the
    domain
    """

    a,b = d
    P = [2]*N
    L = [-1]*(N-1)
    U = [-1]*(N-1)

    #Principal diagonal calculation: O(1)
    P[0] = 1
    P[N-1] = 1

    #Lower diagonal calculation: O(1)
    L[N-2] = 0

    #Upper diagonal calculation: O(1)
    U[0] = 0

    return P,L,U

def driver(N,f,d,alpha,beta):
    """
    This method takes the inputs N(number of points taken in the domain), f the given function,
    the domain d, and the boundary values to give an N-vector of the approximate
    solution to the first degree Poisson equation.
    """

    P,L,U = formMatrix(N,d)
    alpha_vector, gamma_vector = LU_decompose(P,L,U)
    f_vector = buildOutVector(f,d,N,alpha,beta)
    solution = LU_solve(alpha_vector,gamma_vector,f_vector,L)
    return solution

def buildError(N,f,u,d,alpha,beta):
    a,b = d
    diffArr = [1]*N
    exactVal = [1]*N
    approxVal = driver(N,f,d,alpha,beta)
    for i in range(N):
        exactVal[i] = u(a + i*(b-a)/(N-1))
    for i in range(N):
        diffArr[i] = abs(exactVal[i] - approxVal[i])
    error = max(diffArr)
    return error

def genOut(N_array,f,u,d,alpha,beta,out):
    x = [1]*len(N_array)
    y = [1]*len(N_array)
    for i in range(len(N_array)):
        x[i] = math.log(N_array[i])
        y[i] = math.log(buildError(N_array[i],f,u,d,alpha,beta))
    output = open(out,mode='w',encoding='utf-8')
    output.write("X\n")
    for i in range(len(N_array)):
        output.write(str(x[i])+"\n")
    output.write("\n")
    output.write("Y\n")
    for i in range(len(N_array)):
        output.write(str(y[i])+"\n")
    output.close()
    return 0

def test1(N):
    #Method to return approximate value for test case 1
    f = lambda x: 1
    u = lambda x: x/2 - x*x/2
    d = (0,1)
    alpha = 0
    beta = 0
    # print(driver(N,f,d,alpha,beta))
    print(buildError(N,f,u,d,alpha,beta))
    return 0

def test2(N):
    #Method to return approximate value for test case 2
    f = lambda x: math.sin(2*math.pi*x)
    u = lambda x: (math.sin(2*math.pi*x))/(4*math.pi*math.pi)
    d = (0,1)
    alpha = 0
    beta = 0
    # print(driver(N,f,d,alpha,beta))
    print(buildError(N,f,u,d,alpha,beta))
    return 0

def plotTest1(N_array):
    f = lambda x: 1
    u = lambda x: x/2 - x*x/2
    d = (0,1)
    alpha = 0
    beta = 0
    genOut(N_array,f,u,d,alpha,beta,"test1.txt")

def plotTest2(N_array):
    f = lambda x: math.sin(2*math.pi*x)
    u = lambda x: (math.sin(2*math.pi*x))/(4*math.pi*math.pi)
    d = (0,1)
    alpha = 0
    beta = 0
    genOut(N_array,f,u,d,alpha,beta,"test2.txt")

if __name__=="__main__":
    plotTest1([20,40,80,160,320,640,1280,2560,5120,10240])
    plotTest2([20,40,80,160,320,640,1280,2560,5120,10240])
